---
title: Maps
media_order: SystemsChanges_20190524a.svg
date: '16:55 24-05-2019'
---

The rapid evolution of thinking on _Systems Changes_ is better served through maps to support discussion.  After the content matures, a linear presentation slide deck may be more feasible.

The most recent map was presented at the [19th biennial meeting of CANSEE](http://waterloo2019.cansee.ca/) (Canadian Society for Ecological Economics) at the CIGI Campus in Waterloo, Ontario, on May 24, 2019.

* [Index Page](http://systemschanges.com/transfer/20190524_CANSEE/~index.html)
* SVG: [SystemsChanges_20190524a.svg](http://systemschanges.com/transfer/20190524_CANSEE/SystemsChanges_20190524a.svg)
* PDF: [SystemsChanges_20190524a.pdf](http://systemschanges.com/transfer/20190524_CANSEE/SystemsChanges_20190524a.pdf)

Here's a preview of the SVG.
![SystemsChanges_20190524a.svg](SystemsChanges_20190524a.svg)

Prior maps are also available.  This reflect the thinking at the point in time, which may not be completely consistent with the coevolving understanding.

Systems Changes 2019/05/13 at Systems Thinking Ontario
* [Index Page](http://systemschanges.com/transfer/20190513_ST-ON/~index.html)
* SVG: [SystemsChanges_20190513b.svg](http://systemschanges.com/transfer/20190513_ST-ON/SystemsChanges_20190513b.svg)
* PDF: [SystemsChanges_20190513b.pdf](http://systemschanges.com/transfer/20190513_ST-ON/SystemsChanges_20190513b.pdf)

Systems Changes 2019/05/06 at Centre for Social Innovation, Toronto
* [Index Page](http://systemschanges.com/transfer/20190506_CSI/~index.html)
* SVG: [SystemsChanges_20190506a.svg](http://systemschanges.com/transfer/20190506_CSI/SystemsChanges_20190506a.svg)
* PDF: [SystemsChanges_20190506a.pdf](http://systemschanges.com/transfer/20190506_CSI/SystemsChanges_20190506a.pdf)

Systems Changes 2019/04/15 at Centre for Social Innovation, Toronto
* [Index Page](http://systemschanges.com/transfer/20190413_CSI/~index.html)
* SVG: [SystemsChanges_20190415a.svg](http://systemschanges.com/transfer/20190413_CSI/SystemsChanges_20190415a.svg)
* PDF: [SystemsChanges_20190415a.pdf](http://systemschanges.com/transfer/20190413_CSI/SystemsChanges_20190415a.pdf)

Please take these pages with a grain of salt.  The generation of ideas and their meaningful communication is evolving.  Some concepts may not have physically fit on the page on subsequent revisions, while others have been consciously removed.