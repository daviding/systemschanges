---
title: Social_Innovation
date: '19:32 14-09-2018'
---

The prospectus is based on the current understanding expressed in a literature review on social innovation, 

[pdfjs file=201809_SystemsChanges_Ing_SocialInnovationLiteratureReview_v0914a.pdf]
Download as [[PDF v0914a, 3.9MB]](201809_SystemsChanges_Ing_SocialInnovationLiteratureReview_v0914a.pdf)