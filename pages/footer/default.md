---
title: Footer
routable: false
visible: false
---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>, <br />The systemschanges.com domain is CC-BY-NC-SA David Ing 2018-2019, with additional collaborators attributed on specified pages.

This site is powered by [Grav](http://getgrav.org), with the [Bootstrap4 Open Matter](https://github.com/hibbitts-design/grav-theme-bootstrap4-open-matter) theme.

<p>  
                                                                <a class="newwindow external-link" href="https://gitlab.com/daviding/systemschanges/" title="View Content Repository" target="_blank">
      <i class="fa fa-gitlab" aria-hidden="true"></i>
      View Content Repository</a>
      </p>
