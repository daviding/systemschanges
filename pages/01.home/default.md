---
title: Home
media_order: 20090327_MikeCassano_MostInteresingPothole_1500x354_brightness090_gamma050.jpg
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

![](20090327_MikeCassano_MostInteresingPothole_1500x354_brightness090_gamma050.jpg)

In which _systems_ would you like to see _changes_ occur?

The _Systems Changes Learning Circle_ is an open collaborative community, initiated in Toronto, Canada.  A call for participation was launched in January 2019 at the monthly Systems Thinking Ontario meeting.  The web site was will evolve as contributions and knowledge are added.

The plurals in the program name are significant.
* There are multiple system**s** simultaneously at play, not just a single system.
* Change**s** include those within a field that individual and groups can influence, and those in an extended environment that are beyond our abilities.

The program is initially facilitated by David Ing.  Collective learning is encouraged with the cooperation of Systems Changes Learning Circle members.

---

The header image of cobblestone and rail tracks underneath a "[Most interesting pothole](https://www.flickr.com/photos/mdcassano/3396635342)" is CC-BY [Mike Cassano](https://www.flickr.com/photos/mdcassano/) 2009.