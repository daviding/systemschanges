---
title: About
date: '20:32 14-09-2018'
---

![](../home/20090327_MikeCassano_MostInteresingPothole_1500x354_brightness090_gamma050.jpg)

_Systems Changes_ is one of many web venues maintained by David Ing.

The professional site with most activity is Coevolving Innovations, with a [blog](http://coevolving.com/blogs/) and a [publication](http://coevolving.com/commons/publications) section. 

In February 2018, the [Open Innovation Learning](http://openinnovationlearning.com/online/) open access book was released.  This research was developed during doctoral studies in the Department of Industrial Engineering and Management, at Aalto University in Finland.

While there's a [Wikipedia page for David Ing](https://en.wikipedia.org/wiki/David_Ing), a more personal [identity](https://en.wikipedia.org/wiki/David_Ing) page provides more web links.

Find out more at 
* [fa=twitter /] [twitter.com/daviding](https://twitter.com/daviding)
* [fa=linkedin /] [linkedin.com/in/daviding](https://www.linkedin.com/in/daviding/)

If none of these interactions satisfy, you can send a message to David from the [contact page](http://coevolving.com/commons/contact).

