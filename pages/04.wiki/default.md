---
title: Wiki
published: true
date: '12:07 20-07-2019'
visible: true
process:
    markdown: false
    twig: false
iframe_title: 'Three Workshops at ISSS 2019'
display_iframe_title: '0'
iframe_source: 'http://isss2019.daviding.wiki.diglife.coop/view/welcome-visitors/view/introduction-to-the-open-learning-commons-and-the-digital-life-collective'
display_iframe_title_link: '1'
iframe_aspect_ratio: 4by3
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

<p>The Systems Changes Learning Circle is associated with the Open Learning Commons, in exploring federated wiki with pattern language.  See some preliminary writing -- better in
      a full window! -- at: <a href="http://daviding.wiki.openlearning.cc/view/welcome-visitors/view/collections-by-david-ing/wiki.coevolving.com/generative-pattern-language"
        title="http://daviding.wiki.openlearning.cc/view/welcome-visitors/view/collections-by-david-ing/wiki.coevolving.com/generative-pattern-language"
        target="_blank">http://daviding.wiki.openlearning.cc/view/welcome-visitors/view/collections-by-david-ing/wiki.coevolving.com/generative-pattern-language</a></p>
 <iframe src="http://daviding.wiki.openlearning.cc/view/welcome-visitors/view/collections-by-david-ing/wiki.coevolving.com/generative-pattern-language"
  width="1024" height="800"></iframe>