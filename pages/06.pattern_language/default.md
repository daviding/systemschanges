---
title: Pattern_Language
date: '18:19 02-03-2019'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

 _Systems Changes_ aims to reunify some of the [research originating in the 1970s](http://coevolving.com/blogs/index.php/archive/exploring-the-context-of-pattern-languages/) from (i) the systems approach; (ii) wicked problems; and (iii) pattern language (i.e. [C. West Churchman, Horst Rittel and Christopher Alexander](http://coevolving.com/blogs/index.php/archive/christopher-alexander-horst-rittel-c-west-churchman/)).  To move this agenda forward, some appreciation of the development of pattern language from the late 1960s to 2012 can serve as a foundation.
 
The following slide presentation was presented at [Systems Thinking Ontario, February 11, 2019](https://wiki.st-on.org/2019-02-11).
 
[pdfjs file=201902_SystemsChanges_Ing_LearningAlexanderLegacy_v0211a.pdf]
Download as [[PDF v0211a 12.9MB]](201902_SystemsChanges_Ing_LearningAlexanderLegacy_v0211a.pdf)

The abstract and source files are available as "[2019/02/11 Systems Changes: Learning from the Christopher Alexander Legacy](http://coevolving.com/commons/20190211-systems-changes-learning-alexander-legacy)" on the Coevolving Commons.

The web video recording is available at [https://www.youtube.com/watch?v=X3uV4PZPqFw](https://www.youtube.com/watch?v=X3uV4PZPqFw) .
[plugin:youtube](https://www.youtube.com/watch?v=X3uV4PZPqFw)

Footnote:  This presentation only catches readers up on Christopher Alexander's work, and not the fuller agenda of pushing towards an affordance language, see "[2018/03/09 Evolving Pattern Language towards an Affordance Language](http://coevolving.com/commons/20180509-evolving-pattern-language-towards-an-affordance-language)" on the Coevolving Commons.  (Audio was captured, and should be processed for that, too!)