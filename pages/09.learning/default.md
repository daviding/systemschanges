---
title: Learning
date: '19:34 27-08-2018'
---

As we progress, we can share artifacts that may evolve with learning.  

Related, but not necessarily at the right level, is an initial slide set of presented at a CSI Workshop on July 11, 2018.

[pdfjs file=20180711_CSI_Ing_ASystemsApproachOnSocialEnterprise_v0711c.pdf]

Download as [[PDF v0711c, 8.3 MB](20180711_CSI_Ing_ASystemsApproachOnSocialEnterprise_v0711c.pdf)]]