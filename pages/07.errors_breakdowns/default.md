---
title: Errors_Breakdowns
date: '18:19 02-03-2019'
---

An annual lecture in the "Understanding Systems and Systemic Design" course, [Master of Design in Strategic Foresight and Innovation](https://www.ocadu.ca/academics/graduate-studies/strategic-foresight-and-innovation.htm) program at OCAD University is taken as an opportunity to lay down a milestone about the current status of research-in-progress.
 
 The following slide presentation was released to a part-time cohort on March 7, 2019, and to the full-time cohort on March 8.  (Audio was recorded, and a web video presentation will be released, at some date).
 
 [pdfjs file=201903_SystemsChanges_Ing_ErrorsBreakdowns_v0307a.pdf]
Download as [[PDF v0307a 9.8MB]](201903_SystemsChanges_Ing_ErrorsBreakdowns_v0307a.pdf)

The abstract and source files are available as "[2019/03/07 Systems Changes: Errors and Breakdowns; Approaches; Learning](http://coevolving.com/commons/20190307-ocadu-errors-breakdowns)" on the Coevolving Commons.