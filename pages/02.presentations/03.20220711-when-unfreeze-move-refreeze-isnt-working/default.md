---
title: '2022-07-11 When Unfreeze-Move-Refreeze Isn’t Working: Doing, Thinking, and Making via Systems Changes Learning'
media_order: 20201014_RSD9_Khan_ReorderingPrioritiesThroughSystemsChangesLearning.pdf
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2022-07-11 When Unfreeze-Move-Refreeze Isn’t Working: Doing, Thinking, and Making via Systems Changes Learning

<h3>Authors</h3>

<p>David Ing</p>

<h3>Abstract</h3>

<p>Practitioners coming to the systems sciences and/or systems thinking have an interest in “systems change(s)”. Many come from premise that that change occurs as “unfreeze-move-refreeze”, misattributed to Kurt Lewin. A more reflective view of systems changes not only sees distinctions between the intended and emergent recognized by Henry Mintzberg, but also an ecological epistemology. That leads to an appreciation of living systems in a temporality of the landscape as described by Tim Ingold extending the tradition of Gregory Bateson.</p>

<p>Beyond the metaphysics of Heraclitus and Parmenides, the sciences of systems can be extended through a philosophy of science underpinning Classical Chinese Medicine. The ontology of contextual-dyadic thinking outlined by Keekok Lee is in contrast to the Western dualistic thinking.</p>

<p>For novices, the central concepts in the Systems Changes Learning approach are: (i) rhythmic shifts; (ii) texture; and (iii) propensity. Practices are depicted as a hub of “knowing from within”, and four axes of (i) recognizing contextural influences; (ii) diagnosing rhythmic disorders; (iii) prognosing likelihoods; and (iv) reordering pacing.</p>

<p>In workshops, metaphors of (i) sun waxing and waning; (ii) musicians weaving into an ensemble; and (iii) motions riding a surfboard, have effectively reframed thinking about systems changes. Providing a workbook with guiding questions has aided self-organizing groups to surface their points of view and engage in rich discussion, with a minimal coaching by facilitators.</p>

<p>Ongoing action learning on doing (praxis), thinking (episteme) and making (poiesis) by the Systems Changes Learning Circle is in year 4 of an espoused ten-year journey.</p>



<h3>Citation</h3>

<p>David Ing, "When Unfreeze-Move-Refreeze Isn’t Working: Doing Thinking Making Systems Changes", <a href="https://scio.org.uk/"><em>SCiO - Systems and Complexity in Organisation </em></a>, July 11, 2022</p>

<h3>Content</h3>

<ul>
	<li>[<a href="http://systemschanges.com/online/presentations/20220711-when-unfreeze-move-refreeze-isnt-working/20220711_SCiO_Ing_DoingThinkingMakingSystemsChanges_v0710a.pdf">view/download presentation as PDF</a>] (2.2 MB)</li>
</ul>

<br />

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/20220711-when-unfreeze-move-refreeze-isnt-working/20220711_SCiO_Ing_DoingThinkingMakingSystemsChanges_v0710a.pdf&embedded=true"></iframe>
</div>

Web video is available [on Youtube](https://www.youtube.com/watch?v=ERrLeO8HThg).

[plugin:youtube](https://www.youtube.com/watch?v=ERrLeO8HThg)

Download as [[PDF v0710a 18.4MB]](20220711_SCiO_Ing_DoingThinkingMakingSystemsChanges_v0710a.pdf)

Download as [[LibreOffice ODP v0710a 6.9MB]](20220711_SCiO_Ing_DoingThinkingMakingSystemsChanges_v0710a.odp)

[pdfjs file=20220711_SCiO_Ing_DoingThinkingMakingSystemsChanges_v0710a.pdf]