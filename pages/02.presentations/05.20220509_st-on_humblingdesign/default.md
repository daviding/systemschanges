---
title: '2022-05-09 Humbling Design through ‘Systems Changes Learning’'
media_order: 20201014_RSD9_Khan_ReorderingPrioritiesThroughSystemsChangesLearning.pdf
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2022-05-09 Intention or Attention? Humbling Design through ‘Systems Changes Learning’

<h3>Authors</h3>

<p>Zaid Khan</p>

<h3>Abstract</h3>

<p><em>Systems Changes Learning</em> (SCL) is a body of work that offers an updated way of thinking about and responding to change. The three premises of SCL dramatically reposition how we look and understand changes: as <em>rhythms</em> over time that might shift, in accord with the <em>natures</em> of the systems involved.</p>

<p>What could we learn from mapping the SCL premises to designing? This presentation will explore the potential influence that Systems Changes Learning could have on designing and designers, in philosophies, in methods, and in practices.</p>

<p>As a core member of the SCL Circle, Zaid will extend the framing of “humility” - from his Major Research Project “<em>Responding to Complexity with Humility</em>” (2020) - as a lens through which to examine and explore new modes of designing.</p> 

<p>An overarching theme that Zaid examines critically is designing with a bias-towards-(intended)-action, as compared to SCL’s bias-towards-attention. Rather than presuming “interventions” pursuing an idealized end, Zaid wonders what designing would be like if attention was drawn to the nature of rhythmic shifts over time. Could we recontextualize the role and value of designers with our communities by "humbling designing", or "designing with humility"? Taken even further, could this recasting of designing offer opportunities to reconnect the humble features of mainstream philosophies, or perhaps spiritual traditions with designing?</p>

<p>In parallel with the SCL pursuit of critically rethinking systems thinking, is there an opportunity to rethink designing?</p>

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/20220509_st-on_humblingdesign/20220509_ST-ON_Khan_HumblingDesign.pdf&embedded=true"></iframe>
</div>

<h3>Citation</h3>

<p>Zaid Khan, "Intention or Attention? Humbling Design through ‘Systems Changes Learning’", <a href="https://wiki.st-on.org/2022-05-09">Systems Thinking Ontario, May 9, 2022</a>.</p>

<br />

<h3>Content</h3>

<ul>
	<li>[<a href="https://docs.google.com/presentation/d/1xZyT_ga0TTGFL1c2ASNSKo9YydAY3QYtngEdnExTh_o/edit">originally created in Google Slides</a>]</li>
	<li>[<a href="http://systemschanges.com/online/presentations/20220509_st-on_humblingdesign/20220509_ST-ON_Khan_HumblingDesign.pdf">view/download slides as PDF</a>] (8.5 MB)</li>
</ul>

[pdfjs file=20220509_ST-ON_Khan_HumblingDesign.pdf]