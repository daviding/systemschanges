---
title: '2020-1Q OCADU SFI'
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2020-1Q OCADU SFI

The state-of-the-art in our thinking sometimes shows up in presentations, for invited audiences or for graduate school courses.

In winter 2020, a series for four lectures were prepared for the [Master's program in Strategic Foresight & Innovation at OCADU](http://www.ocadu.ca/academics/graduate-studies/strategic-foresight-and-innovation).  These are available as [a series of blog posts](http://coevolving.com/blogs/index.php/archive/tag/ocadu-sfi/), and as [a playlist on Youtube](https://www.youtube.com/playlist?list=PLCUdZD8xQ449N_E0e4xxb2U4YVdn3pjIu).

On January 17, 2020, the lecture focused on the shift towards systems changes, from system + change as independent parts.

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/c701A0LxcEg"></iframe>
</div>

* The session outline, presentation slides and downloadable audio are available as "Are Systems Changes Different from System + Change?" | January 23, 2020 | Coevolving Innovations at [http://coevolving.com/blogs/index.php/archive/are-systems-changes-different-from-system-change/](http://coevolving.com/blogs/index.php/archive/are-systems-changes-different-from-system-change/) .

On January 31, 2020, the lecture surfaced errors, attention and traps (from an ecological understanding).

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/wBIzFxA-OgU"></iframe>
</div>

* The session outline, presentation slides and downloadable audio are available as "Why (Intervene in) Systems Changes?" | February 15, 2020 | Coevolving Innovations at [http://coevolving.com/blogs/index.php/archive/why-intervene-in-systems-changes/](http://coevolving.com/blogs/index.php/archive/why-intervene-in-systems-changes/) .

On February 7, 2020, the lecture reframed episteme, techne and phronesis, diving into appreciative systems, services systems, and the socio-technical systems perspective).

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PZUlcCqChiE"></iframe>
</div>


* The session outline, presentation slides and downloadable audio are available as "Whom, when + where do Systems Changes situate?" | February 12, 2020 | Coevolving Innovations at [http://coevolving.com/blogs/index.php/archive/whom-when-where-do-systems-changes-situate/](http://coevolving.com/blogs/index.php/archive/whom-when-where-do-systems-changes-situate/) .

On February 6, 2020, the lecture presented an alternative to the espoused unfreeze-change-refreeze steps (attributed wrongly to Kurt Lewin), directing attention towards more recent advances in situated learning + history-making, and commitment and language-action perspective).

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/0RbEG3NiNbw"></iframe>
</div>

* The session outline, presentation slides and downloadable audio are available as "How do Systems Changes become natural practice?" | March 18, 2020 | Coevolving Innovations at [http://coevolving.com/blogs/index.php/archive/how-do-systems-changes-become-natural-practice/](http://coevolving.com/blogs/index.php/archive/how-do-systems-changes-become-natural-practice/) .