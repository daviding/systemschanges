---
title: Presentations
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# Presentations

Since the Systems Change Learning Circle was founded on a horizon of 10 years, ongoing work is subject to revision.

Here's a list of presentations, from the most recent, to the nascent:

<table>
    
      <tr>
        <td><a href="http://systemschanges.com/online/presentations/20221017-reifying-systems-thinking-towards-changes">2022-10-10 Systems Thinking Ontario</a></td>
          <td>Reifying Systems Thinking towards Changes: Rhythmic Shifts, (Con)Texture, and Propensity amongst Living Systems</td>
    </tr>
    
    <tr>
        <td><a href="http://systemschanges.com/online/presentations/20221010-knowing-better-via-systems-thinking">2022-10-10 Universitat de Barcelona Business School</a></td>
        <td>Knowing Better via Systems Thinking: Traditions and Contemporary Approaches</td>
    </tr>
    
    <tr>
        <td><a href="http://systemschanges.com/online/presentations/20220711-when-unfreeze-move-refreeze-isnt-working">2022-07-11 SCiO - Systems and Complexity in Organisation </a></td>
        <td>When Unfreeze-Move-Refreeze Isn’t Working: Doing, Thinking, and Making via Systems Changes Learning
’</td>
    </tr>
    
       <tr>
        <td><a href="http://systemschanges.com/online/presentations/20220509_st-on_humblingdesign">2022-05-09 Systems Thinking Ontario</a></td>
        <td>Intention or Attention? Humbling Design through ‘Systems Changes Learning’</td>
    </tr>
   <tr>
        <td><a href="http://systemschanges.com/online/presentations/20220304_cfc">2022-03-04 Code for Canada</a></td>
        <td>Systems Thinking through Changes (for the Canadian Digital Service)</td>
    </tr>
       <tr>
        <td><a href="http://systemschanges.com/online/presentations/20211105-rsd10-friends-or-foes">2021-11-05 Relating Systems Thinking and Design 10</a></td>
        <td>Friends or Foes: Theory of Change, Systemic Design (Thinking), Systems Changes Learning</td>
    </tr>
    <tr>
        <td><a href="http://systemschanges.com/online/presentations/20201022_gcd">2020-10-22 Global Change Days</a></td>
        <td>Learning With Humility: Systems Thinking and Reordering Priorities</td>
    </tr>
    <tr>
        <td><a href="http://systemschanges.com/online/presentations/20201019_st-on">2020-10-19 Systems Thinking Ontario</a></td>
        <td>Theoretical Grounds, Pragmatic Grounds: Methods for Reordering our Priorities through Systems Changes Learning</td>
    </tr>
        <tr>
        <td><a href="http://systemschanges.com/online/presentations/20201014_rsd9">2020-10-14 RSD9</a></td>
        <td>Reordering our Priorities through Systems Change Learning</td>
                <tr>
        <td><a href="http://systemschanges.com/online/presentations/2020-1q_ocadu_sfi">2020-1Q OCADU SFI</a></td>
                    <td><ul><li>Are Systems Changes Different from System + Change?</li>
                   <li>Why (Intervene in) Systems Changes?</li>
                    <li>Whom, when + where do Systems Changes situate?</li>
                        <li>How do Systems Changes become natural practice?</li>
            </ul></td>
    </tr>
        <tr>
        <td><a href="http://systemschanges.com/online/presentations/20190114_st-on">2019-01-14 Systems Thinking Ontario</a></td>
        <td>Systems Changes:  A Call for Participation</td>
    </tr>

</table>