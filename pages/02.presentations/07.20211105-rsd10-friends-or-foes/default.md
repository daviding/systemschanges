---
title: '2021-11-05 Friends or Foes: Theory of Change, Systemic Design (Thinking), Systems Changes Learning'
media_order: 202203_CodeForCanada_SystemsThinkingThroughChanges_CC-BY-SA_v0304a.pdf
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2021-11-05 Friends or Foes: Theory of Change, Systemic Design (Thinking), Systems Changes Learning

<h3>Authors</h3>

<p>Zaid Khan + David Ing</p>

<h3>Abstract</h3>

<p>In this dialogue, we will facilitate a discussion amongst RSD10 participants on the compatibilities and incompatibilities between (i) Theory of Change (ToC), (ii) Systemic Design (Thinking) (SDT) and (ii) Systems Change(s) Learning (SCL).</p>

<blockquote>As a trigger question, we will start with: Do ToC, SDT and SCL overlap to a greater or less extent? Can or should that overlap see further integration or separation?</blockquote>

<p>The three approaches have been discussed as separate topics in prior RSD meetings.</p>

<ul>
	<li>At RSD9, Peter Jones reviewed&nbsp;<a href="https://rsdsymposium.org/peter-jones/">current uses of Theory of Change</a>&nbsp;in practice, in negotiations between (philanthropic) funders and changemakers. Oriented towards linear logic models, simplistic presentations may not well represent the complexities of aspirations for change.</li>
	<li>Systemic design has been at the core of RSD meetings, originating from the popularization of design thinking (e.g. from IDEO) (Brown, 2008), into the body of work now shepherded by the Systemic Design Association. The&nbsp;<a href="https://www.systemicdesigntoolkit.org/">Systemic Design Toolkit</a>&nbsp;(Van Ael et al., 2018) has emphasized practical frameworks and designerly methods (Jones, 2018). The systems turn with design enlarges the vision from the heritage in products, through services, into complex social systems (Jones, 2017).</li>
	<li>Systems changes learning has been introduced in two previous RSD meetings, coming from the systems sciences community (Khan, 2019, Khan, Ing, et al., 2020). The popularization of organisations seeking “systems change” may be systemic or systematic in nature. Foundations may be espoused in systems thinking, cybernetics or complexity science, yet that appreciation for ways in which wicked problems in the present are transitioned into better outcomes in the future is not always clear. The systems changes learning approach appreciates natures that may require reframing beyond anthropocentric presuppositions.</li>
</ul>

<p>We welcome a dialogue to explore the variety of perspectives and understandings on ways in which a synthesis of ToC, SDT and SCL is possible and/or desirable.</p>

<h4>Format</h4>

<p><em>Moderation</em>: Zaid Khan and David Ing</p>

<p><em>Agenda</em>: Introduce concepts Suggested questions for dialogue Group dialogue Summary Facilitation: members of Systems Changes Learning Circle This workshop will be led by members of the Systems Changes (SC) Learning Circle – based out of Toronto, Canada. Started in 2019, the Circle is on a 10-year journey to develop methods based on multiparadigm inquiry that integrates a variety of schools of thought. On our journey, the Circle has previously shared its progress at RSD8 (Khan &amp; Ing, 2019) and RSD9 (Ing, Khan et al., 2020).</p>

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/20211105-rsd10-friends-or-foes/20211105_RSD10_Khan_Ing_FriendsOrFoes.pdf&embedded=true"></iframe>
</div>

<h3>Citation</h3>

<p>Zaid Khan + David Ing, "Friends or foes: Theory of Change, Systemic Design (Thinking), Systems Changes Learning", <a href="https://rsdsymposium.org/friends-or-foes-theory-of-change-systemic-design-thinking-and-systems-changes-learning/">Relating Systems and Design (RSD) 10</a>, TU Delft.</p>

<br />

<h3>Content</h3>

<ul>
	<li>[<a href="https://docs.google.com/presentation/d/1N4GN0JyWNz4Yjnb1VXupBVH-FbLsrMJE6LeUEh-ZHFI/edit">originally created in Google Slides</a>]</li>
	<li>[<a href="http://systemschanges.com/online/presentations/20211105-rsd10-friends-or-foes/20211105_RSD10_Khan_Ing_FriendsOrFoes.pdf">view/download slides as PDF</a>] (4.1 MB)</li>
</ul>

[pdfjs file=20211105_RSD10_Khan_Ing_FriendsOrFoes.pdf]