---
title: '2020-10-22 Global Change Days'
media_order: '20201020_LearningWithHumility_v0920a.pdf,20201020_LearningWithHumility_v0920a.odp'
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2020-10-22 Global Change Days

In October, members of the Systems Changes Learning Circle led three sessions:
* at [Relating Systems Thinking and Design (RSD9) conference](https://rsdsymposium.org/2020/09/reordering-our-priorities-through-systems-change-learning/), October 14; 
* at [Systems Thinking Ontario (ST-ON)](https://wiki.st-on.org/2020-10-19), October 19; and
* at [Global Change Days Beacon Events](https://globalchangedays.com/beacons/#beacon4) October 22.  
These collectively reprepsent an evolving core knowledge set under Creative Commons licensing, being deployed in different styles for different audiences.

Below is the content of the third of three workshops, oriented towards an audience of changemakers.

## Learning With Humility:  Systems Thinking and Reordering Priorities

This interactive beacon session will engage change makers to think differently, to explore their relationship to learning.

The breakout sessions will provide participants an opportunity to explore the Systems Thinking questions: the urgent vs the important, the local vs. the distant, problem solving vs history-making. Finally the audience will be invited to review their self-reflections and the potential re-ordering of their priorities, to make a difference.

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/20201022_gcd/20201020_LearningWithHumility_v0920a.pdf&embedded=true"></iframe>
</div>

Download as [[PDF v0920a 4.1MB]](20201020_LearningWithHumility_v0920a.pdf)

Download as [[LibreOffice ODP v0920a 14.92MB]](20201020_LearningWithHumility_v0920a.odp)

[pdfjs file=20201020_LearningWithHumility_v0920a.pdf]
