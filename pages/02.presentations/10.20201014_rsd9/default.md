---
title: '2020-10-14 RSD9'
media_order: 20201014_RSD9_Khan_ReorderingPrioritiesThroughSystemsChangesLearning.pdf
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2020-10-14 RSD9

In October, members of the Systems Changes Learning Circle led three sessions:
* at [Relating Systems Thinking and Design (RSD9) conference](https://rsdsymposium.org/2020/09/reordering-our-priorities-through-systems-change-learning/), October 14; 
* at [Systems Thinking Ontario (ST-ON)](https://wiki.st-on.org/2020-10-19), October 19; and
* at [Global Change Days Beacon Events](https://globalchangedays.com/beacons/#beacon4) October 22.  

These collectively reprepsent an evolving core knowledge set under Creative Commons licensing, being deployed in different styles for different audiences.

Below is the content of the first of three workshops, oriented towards an audience of designers.

## Reordering our Priorities through Systems Change Learning

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/20201014_rsd9/20201014_RSD9_Khan_ReorderingPrioritiesThroughSystemsChangesLearning.pdf&embedded=true"></iframe>
</div>

Download as [[PDF 1.6MB]](20201014_RSD9_Khan_ReorderingPrioritiesThroughSystemsChangesLearning.pdf)

The original presentation can be viewed [on Google Slides](https://docs.google.com/presentation/d/1OpbXr2bykI-uwKu3-9RN82hXEFekPdoOe7gDcwJ6_QE/edit?usp=sharing).

[plugin:youtube](https://www.youtube.com/watch?v=vxaapSG49Hk)

Web video is available [on Youtube](https://www.youtube.com/watch?v=vxaapSG49Hk).


## Abstract

* Zaid Khan, David Ing, Dan Eng, Kelly Okamura, Zemina Meghji

The idea of "systems change" has risen in popularity over the past few years.  To make this more than just another buzzword, how might we approach it?  In what ways does "systems change" mean more than just "change"?  Does "systems change" build on the large legacy of "systems thinking"?

The *Systems Changes Learning Circle* is now in year 2 of 10 year journey.  Our aim is to reify *systems changes* as a first-class concept.  This extends prior published research on social and organizational change, based in the systems sciences.  At RSD8, the Khan and Ing (2019) presentation reflected the early explorations coming from the core group.  For 2020, at RSD9, we propose a workshop to share some methods in early stages of development for initiating deeper deliberations into systems changes.

Systems changes may involve:
* shifting adaptively;
* shifting behaviorally; and/or 
* shifting ecologically.  

Living systems may respond through:
* systematic changes;
* systemic changes; and/or
* timescape-landscape changes.

Degrees of systems changes may be judged as:
* unfolding nature;
* fixing problems; or 
* history making.

The multi-day, iterative workshop still under development takes a multi-paradigm approach based on learnings grounded in five philosophies:  

* (i) learning *which*, as phenomenology; 
* (ii) learning *what*, as ontology; 
* (iii) learning *why*, as epistemology; 
* (iv) learning *whom+when+where*, as phronesis; and 
* (v) learning *how*, as techne.  

To convene working groups in advance of iterations on the five learnings, we now propose a Question Zero conversation for orientation, on Reordering Priorities.  This workshop on Reordering Priorities can be conducted within 90 minutes, in-person or online, with parallel breakout groups.

The workshop will be structured as multiple steps:

* Step 0: Participants will introduce themselves briefly.  To encourage discussion, participants will be encouraged to cluster into small groups with others whom they do not know well.
* Step 1:  As individuals, participants will each quickly jot down three top three systems changes in which they are interested.  These systems changes may be ones that they would like to come faster, or ones they would like to not happen.
* Step 2:  A two-dimensional map will be presented.  Individuals will be asked to place their top three systems changes with axes along:
    * urgent to important; and
    * local to distant (Pepper, 1934; Tolman and Brunswik, 1935).
* Step 3: In groups, each individual will be asked to show their mappings, and provide background for having prioritized those interests.
* Step 4: Authentic systems thinking orders synthesis (putting thing together) before analysis (taking things apart).  Each group will be encouraged to attempt to synthesize its priorities across its participants.
* Step 5:  One reporter from each group will reflect on their collective experience on reordering priorities.

Artifacts and comments from the group reports will be collected for summarization, possibly for publication in the proceedings. This knowledge-creating exercise will be used to refine methods for groups engaging in action learning. 

The *Systems Changes Learning Circle* (founded 2019) is a group convening at the *Centre for Social Innovation* (Toronto) emerging from *Systems Thinking Ontario* (founded 2012).  We include postgraduates and instructors from the Strategic Foresight and Innovation Program at OCADU in Toronto.  Our content at licensed as Creative Commons at [http://systemschanges.com](http://systemschanges.com) .  We cooperate with the Open Learning Commons at [http://openlearning.cc](http://openlearning.cc) , and the Digital Life Collective at [http://diglife.com](http://diglife.com) .

### References

Emery, Fred E., and Eric L. Trist. 1965. “The Causal Texture of Organizational Environments.” *Human Relations* 18 (1): 21–32. [https://doi.org/10.1177/001872676501800103](https://doi.org/10.1177/001872676501800103).


Ing, David. 2013. “Rethinking Systems Thinking:  Learning and Coevolving with the World.” *Systems Research and Behavioral Science* 30 (5): 527–47. [https://doi.org/10.1002/sres.2229](https://doi.org/10.1002/sres.2229).

Ing, David. 2017. *Open Innovation Learning: Theory Building on Open Sourcing While Private Sourcing*. Toronto, Canada: Coevolving Innovations Inc. [https://doi.org/10.20850/9781775167211](https://doi.org/10.20850/9781775167211).


Khan, Zaid, and David Ing. 2019. “Paying Attention to Where Attention Is Placed in the Rise of System(s) Change(s).” In *Proceedings of the RSD8 Symposium*. IIT -- Institute of Design, Chicago, Illinois: Systems Design Association. [https://systemic-design.net/rsd-symposia/rsd8-2019/systems-change/](https://systemic-design.net/rsd-symposia/rsd8-2019/systems-change/).

Pepper, Stephen C. 1934. “The Conceptual Framework of Tolman’s Purposive Behaviorism.” *Psychological Review* 41 (2): 108–33. [https://doi.org/10.1037/h0075220](https://doi.org/10.1037/h0075220).

Ramírez, Rafael, John W Selsky, and Kees van der Heijden. 2008. “Conceptual and Historical Overview.” In *Business Planning for Turbulent Times: New Methods for Applying Scenarios*, edited by Rafael Ramírez, John W. Selsky, and Kees van der Heijden, 17–30. Earthscan. [http://doi.org/10.4324/9781849774703](http://doi.org/10.4324/9781849774703).

Tolman, Edward C., and Egon Brunswik. 1935. “The Organism and the Causal Texture of the Environment.” *Psychological Review* 42 (1): 43. [https://doi.org/10.1037/h0062156](https://doi.org/10.1037/h0062156).

[pdfjs file=20201014_RSD9_Khan_ReorderingPrioritiesThroughSystemsChangesLearning.pdf]
