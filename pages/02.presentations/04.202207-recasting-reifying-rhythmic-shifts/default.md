---
title: '2022-07 Recasting and Reifying Rhythmic Shifts'
media_order: 20201014_RSD9_Khan_ReorderingPrioritiesThroughSystemsChangesLearning.pdf
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2022-07 Systems Changes Learning: Recasting and reifying rhythmic shifts for doing, alongside thinking and making

<h3>Authors</h3>

<p>David Ing</p>

<h3>Abstract</h3>

<p>In 2022, the Systems Changes Learning Circle is in its fourth year of 10-year journey on “Rethinking Systems Thinking”.  In a contextural action learning approach, the Circle has elevated rhythmic shifts as the feature that both resonates with practitioners in the field, and fits with a post-colonial philosophy of science bridging classical Chinese thought with Western professional practices.  This multiparadigm inquiry recasts and reifies the activities of doing (praxis), thinking (theoria) and making (poiesis).  The facility with this approach is deepened through three levels: (i) educating of attention, orienting novices towards contrasting modes of thought; (ii) learning for co-relating, lending a way for practitioners to critically appreciate their situations, and (iii) learning for articulating, aiding mentors to guide groups productively through mutual learning.</p>

<h3>Citation</h3>

<p>David Ing, "Systems Changes Learning: Recasting and reifying rhythmic shifts for doing, alongside thinking and making", <a href="http://journalofssb.com/index.php/ssb"><em>The Journal of Sustaiable Smart Behavior</em></a>, August 2022, in press</p>

<h3>Content</h3>

<ul>
	<li>[<a href="http://systemschanges.com/online/presentations/202207-recasting-reifying-rhythmic-shifts/2022_SSB_Ing_SystemsChangesLearning_v0624a.pdf">view/download presentation as PDF</a>] (2.2 MB)</li>
</ul>

<!-- bootstrap responsive 297x210 aspect ratio-->
<div class="embed-responsive embed-responsive-297by210" style="padding-bottom: 141.42%;">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/202207-recasting-reifying-rhythmic-shifts/2022_SSB_Ing_SystemsChangesLearning_v0624a.pdf&embedded=true">
  </iframe>
</div>

[pdfjs file=2022_SSB_Ing_SystemsChangesLearning_v0624a.pdf]