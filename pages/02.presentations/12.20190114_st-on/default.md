---
title: '2019-01-14 Systems Thinking Ontario'
media_order: 201901_SystemsChanges_Ing_CfP_v0114a.pdf
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2019-01-14 Systems Thinking Ontario

The activities on _Systems Changes_ have largely emerged from participation amongst the collaborators.  This began in _A Call for Participation_, that was presented at [Systems Thinking Ontario, January 14, 2019](https://wiki.st-on.org/2019-01-14).

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=https://systemschanges.com/online/presentations/20190114_st-on/201901_SystemsChanges_Ing_CfP_v0114a.pdf&embedded=true"></iframe>
</div>

Download as [[PDF v0114a 3.4MB]](201901_SystemsChanges_Ing_CfP_v0114a.pdf)

The abstract and source files are available as "[2019/01/14 Systems Changes: A Call for Participation](http://coevolving.com/commons/20190114-systems-changes-call)" on the Coevolving Commons.

[pdfjs file=201901_SystemsChanges_Ing_CfP_v0114a.pdf]