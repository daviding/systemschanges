---
title: '2020-10-19 Systems Thinking Ontario'
media_order: '20201019_ReorderingPriorities_v1019a.pdf,20201019_ReorderingPriorities_v1019a.odp'
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2020-10-19 Systems Thinking Ontario

In October, members of the Systems Changes Learning Circle led three sessions:
* at [Relating Systems Thinking and Design (RSD9) conference](https://rsdsymposium.org/2020/09/reordering-our-priorities-through-systems-change-learning/), October 14; 
* at [Systems Thinking Ontario (ST-ON)](https://wiki.st-on.org/2020-10-19), October 19; and
* at [Global Change Days Beacon Events](https://globalchangedays.com/beacons/#beacon4) October 22.  

These collectively reprepsent an evolving core knowledge set under Creative Commons licensing, being deployed in different styles for different audiences.

Below is the content of the second of three workshops, oriented towards a systems thinking community that has convened monthly for over 5 years.

## Theoretical Grounds, Pragmatic Grounds:  Methods for Reordering our Priorities through Systems Changes Learning

Methods development has progressed with two interacting teams with contrasting emphases:

* the scholarly team, focused on correctness, validating on theoretical grounds; and
* the field team, focused on understandability, validating on pragmatic grounds.

For [this Systems Thinking Ontario meeting](https://wiki.st-on.org/2020-10-19), David Ing will review the introductory framing on which the circle has converged, and outline the sequence of activities expected ahead.

(This session includes revisions following "[2020-10-14 RSD9 -- Reordering our Priorities through Systems Change Learning](http://systemschanges.com/online/presentations/20201014_rsd9)".

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/20201019_st-on/20201019_ReorderingPriorities_v1019a.pdf&embedded=true"></iframe>
</div>

Download as [[PDF v1019a 8.1MB]](20201019_ReorderingPriorities_v1019a.pdf))

Download as [[LibreOffice ODP v1019a 25.9MB]](20201019_ReorderingPriorities_v1019a.odp)

[pdfjs file=20201019_ReorderingPriorities_v1019a.pdf]
