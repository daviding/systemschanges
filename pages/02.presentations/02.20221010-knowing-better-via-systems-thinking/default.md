---
title: '2022-10-10 Knowing Better via Systems Thinking: Traditions and Contemporary Approaches'
media_order: 20201014_RSD9_Khan_ReorderingPrioritiesThroughSystemsChangesLearning.pdf
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2022-10-10 Knowing Better via Systems Thinking: Traditions and Contemporary Approaches

<h3>Authors</h3>

<p>David Ing</p>

<h3>Abstract</h3>

<p>Systems thinking can be seen as a superpower that enables individuals and groups to work through wicked messes. Authentic systems thinking has a long tradition in the systems sciences, with variety of schools of thought. Contemporary development of interdisciplinary approaches in the 21st century include service systems science and resilience science. Based, in Toronto, Canada, the Systems Changes Learning Circle is contributing new practices, theories and methods in a post-colonial philosophy of science..</p>

<h3>Citation</h3>

<p>David Ing, "Knowing Better via Systems Thinking: Traditions and Contemporary Approaches", <a href="http://grad.ub.edu/grad3/plae/AccesInformePD?curs=2022&codiGiga=364557&idioma=CAT&recurs=publicacio"><em>Universitat de Barcelona Business School, International Operations Management class </em></a>, July 11, 2022</p>

<h3>Content</h3>

<ul>
	<li>[<a href="http://systemschanges.com/online/presentations/20221010-knowing-better-via-systems-thinking/20221010_UBarcelona_Ing_KnowingBetterViaSystemsThinking_v1008a.pdf">view/download presentation as PDF</a>] (6.5 MB)</li>
</ul>

<br />

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/20221010-knowing-better-via-systems-thinking/20221010_UBarcelona_Ing_KnowingBetterViaSystemsThinking_v1008a.pdf&embedded=true"></iframe>
</div>

Web video is available [on Youtube](https://www.youtube.com/watch?v=h0UJVjPpf2U).

[plugin:youtube](https://www.youtube.com/watch?v=h0UJVjPpf2U)

Download as [[PDF v1010a 6.5MB]](20221010_UBarcelona_Ing_KnowingBetterViaSystemsThinking_v1008a.pdf)

Download as [[LibreOffice ODP v1010a 14.8MB]](20221010_UBarcelona_Ing_KnowingBetterViaSystemsThinking_v1008a.odp)

[pdfjs file=20221010_UBarcelona_Ing_KnowingBetterViaSystemsThinking_v1008a.pdf]