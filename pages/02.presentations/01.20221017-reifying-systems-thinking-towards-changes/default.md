---
title: '2022-10-17 Reifying Systems Thinking towards Changes'
media_order: 20201014_RSD9_Khan_ReorderingPrioritiesThroughSystemsChangesLearning.pdf
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2022-10-17 Reifying Systems Thinking towards Changes: Rhythmic Shifts, (Con)Texture, and Propensity amongst Living Systems

<h3>Authors</h3>

<p>David Ing</p>

<h3>Abstract</h3>

<p>In 2012, at the 56th Annual Meeting of the International Society of the Systems Sciences, an aspiration of "Rethinking Systems Thinking" was proposed. In 2019, the rise in interest in "systems change" led to the formation of the Systems Changes Learning Circle, centered in Toronto, Canada. Now 4 years into a 10-year journey, research publications and presentations are being released..</p>

<p>In 2022, the Systems Changes Learning approach features three concepts: (i) rhythmic shifts; (ii) texture (leading to contexture); and (iii) propensity. Practices developed are depicted as hub of "knowing from within" appreciated through a cycle of learning along four spoke. Theory-building through multiparadigm inquiry includes philosophies of science underlying Classcial Chinese Medicine and ecological anthropology.</p>

<h3>Citation</h3>

<p>David Ing, "Reifying Systems Thinking towards Changes: Rhythmic Shifts, (Con)Texture, and Propensity amongst Living Systems", <a href="https://wiki.st-on.org/2022-10-17"><em>Systems Thinking Ontario</em></a>, October 17, 2022</p>

<h3>Content</h3>

<ul>
	<li>[<a href="http://systemschanges.com/online/presentations/20221010-knowing-better-via-systems-thinking/20221010_UBarcelona_Ing_ReifyingSystemsThinkingTowardsChanges_v1008a.pdf">view/download presentation as PDF</a>] (8.5 MB)</li>
</ul>

<br />

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/20221017-reifying-systems-thinking-towards-changes/20221010_UBarcelona_Ing_ReifyingSystemsThinkingTowardsChanges_v1008a.pdf&embedded=true"></iframe>
</div>

Web video is available [on Youtube](https://www.youtube.com/watch?v=UK2PgJxv7V0).

[plugin:youtube](https://www.youtube.com/watch?v=UK2PgJxv7V0)

Download as [[PDF v1008a 8.5MB]](20221010_UBarcelona_Ing_ReifyingSystemsThinkingTowardsChanges_v1008a.pdf)

Download as [[LibreOffice ODP v1008a 23.0MB]](20221010_UBarcelona_Ing_ReifyingSystemsThinkingTowardsChanges_v1008a.odp)

[pdfjs file=20221010_UBarcelona_Ing_ReifyingSystemsThinkingTowardsChanges_v1008a.pdf]