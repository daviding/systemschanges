---
title: '2022-03-04 Code for Canada'
media_order: 202203_CodeForCanada_SystemsThinkingThroughChanges_CC-BY-SA_v0304a.pdf
date: '18:49 14-09-2018'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

# 2022-03-04 Code For Canada

For the Canadian Digital Service, via Code for Canada, members of the Systems Changes Learning conducted a workshop

[plugin:youtube](https://www.youtube.com/watch?v=nu2ExVCkJJE)

This content was created under a Creative Commons CC-BY-SA license.

<!-- bootstrap responsive 16:9 aspect ratio-->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/gview?url=http://systemschanges.com/online/presentations/20220304_cfc/202203_CodeForCanada_SystemsThinkingThroughChanges_CC-BY-SA_v0304a.pdf&embedded=true"></iframe>
</div>

Download as [[PDF v0304a 4.4MB]](202203_CodeForCanada_SystemsThinkingThroughChanges_CC-BY-SA_v0304a.pdf)

Download as [[LibreOffice ODP v0304a 6.5MB]](202203_CodeForCanada_SystemsThinkingThroughChanges_CC-BY-SA_v0304a.odp)

## Systems Thinking through Changes

### Presentation
**Date**: Friday, March 4th, 2022 at 11am ET

**Format**: 1 hour presentation / panel for the entire CDS Team
 
**Speakers**:  
* David Ing, Co-Founder Systems Changes Learning Circle
 
#### Overview: 

There are a variety of approaches to systems thinking in practice.

_Systems Changes Learning_ takes a view of living systems over time.  As individuals, groups and organizations, we are lines (lifelines) co-responding alongside each other.  Our lines have rhythms. Sometimes, the lines weave together in synchrony.  At other times, the lines might clash, or get tangled up.

Interest in systems thinking arises when people feel “stuck”.  A system could be trapped in a rut, or struggling through an abrupt transformational change.  We look into dysfunctions as rhythmic shifts in the primary system of interest, and/or in co-related systems of influence.  When the path from action to outcome is linear, a solution is straightforward.  When a system of problems is a complex mess, building a deeper appreciation as a group may open a route to getting unstuck.

The presentation will introduce learners to this perspective.

### Workshop Group Session
**Date**: Friday, March 4th, 2022 at 1pm ET

**Format**: 3 hour workshop (Two 90-min sessions)  / Groups of 3-5
 
**Speakers / Facilitators**:  
* David Ing and co-founders of the Systems Changes Learning Circle (Zaid Khan, Dan Eng and Kelly Okamura)

Approach:
 
* Choose one individual who will serve as editor for the proceedings of the session.
* We will jointly step through some templates that will guide our collective learning.
* Discuss the position statements by each contributor, and construct a collective appreciation of the situation.
* Reserve time to converge on some shared sense of progress made, so that writing up retrospective proceedings will be easier.

### Workshop Preparation
Learning to do, think and make in systems is a journey.  Forming groups with individuals who share a system of interest (or a very few co-related systems) improves relevance.  With a common mindset, the group is more likely to find a collective path forward.
 
Here is an outline of expected activities prior to the workshop on March 4th 2022:

1. Form a group of 3 to 5. Ideally these are your teammates who share similar familiarity of domain / problem space. 
2. Confirm your group membership for the scheduled working session with EE by Monday February 28 (and via this form).
3. Write a position statement (one or two paragraphs) responding to the following by Monday Feb 28 2022 (and via this form):

  a. Name
  
  b. Group
  
  c. What are the 3 rhythmic shifts most present to you, in your primary system of interest (i.e. project or initiative)
  
  d. What are some rhythmic shifts in co-related systems of influence
  
4. Attend the scheduled presentation in person on Friday March 4th and participate with clarifying questions.  Otherwise, watch the recording in replay.
 
### Contributing to Retrospective Proceedings
Write one or two paragraphs, and send to the proceedings editor, responses to the questions:
 
* What did we learn in the working session?
* Which questions call for extended investigating?
* When and where might we follow up with next steps with action?
 
Retrospective proceedings are not necessarily findings or recommendations.  They are a record of the time spent together, so that others may learn from your collective experience.
 
In an action learning, the steps above may be repeated in a cycle, either with or without external guidance.

### Facilitators
Registration for this CDS program is coordinated through EE.
 
The cofounders of the Systems Changes Learning Circle are David Ing, Zaid Khan, Dan Eng and Kelly Okamura.  They may be contacted via RB at Code for Canada.

[pdfjs file=202203_CodeForCanada_SystemsThinkingThroughChanges_CC-BY-SA_v0304a.pdf]